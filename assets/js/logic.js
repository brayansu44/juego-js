console.log("*Adivina Quien Soy*");

let personajes = [{
	name:['Leon Scott Kennedy','lion','leon', 'Lion', 'Leon', 'leon scott kennedy'],
	foto:'leon.jpg',
	preguntas:['Es un experto en armas?','Fue policia?','Tiene novia?','Trabaja para el gobierno de los Estados unidos?','Esta muerto?'],
	respuestas:['si','si','no','si','no'],
}, {
	name:['Madara Uchiha', 'madara', 'Madara' ,'madara uchiha'],
	foto:'madara.jpg',
	preguntas:['Pertene al clan Senju?','Vuela?','Posee el mangekyou sharingan?','Es Fuerte?','Provoco la cuarta guerra ninja?'],
	respuestas:['no','no','si','si','si'], 
}, {
	name:['Kratos', 'kratos', 'fantasma de sparta' ,'el dios de la guerra',],
	foto:'kratos.jpg',
	preguntas:['Es calvo?','Es un dios?','Asesino a su hermano?','Ha muerto?','Fue un dios?'],
	respuestas:['si','no','no','si','si'], 
}, {
	name:['dante', 'Dante', 'devil may cry', 'hijo de sparda'],
	foto:'dante.jpg',
	preguntas:['Sus padres estan vivos?','Tiene el pelo de color blanco?','Tiene hermanos?','Es malo?','Lucha contra demonios?'],
	respuestas:['no','si','si','no','si'],
}, {
	name:['nemesis', 'Nemesis'],
	foto:'nemesis.jpeg',
	preguntas:['Es humano?','Fue humano?','Es malo?','Su principal objetivo es asesinar a todos los miembros de S.T.A.R.S?','Esta vivo?'],
	respuestas:['no','si','si','si','no'],
}

];

const btnJugar = document.getElementById("btnJugar");
const imagenPersonaje = document.getElementById("imagenPersonaje");

let indice = parseInt(Math.random() * 5);
let opacidad = 20;
let puntaje = 0;

btnJugar.addEventListener('click', () =>{

	const pregunta0 = document.getElementById("pregunta0");
	const pregunta1 = document.getElementById("pregunta1");
	const pregunta2 = document.getElementById("pregunta2");
	const pregunta3 = document.getElementById("pregunta3");
	const pregunta4 = document.getElementById("pregunta4");
	
	 imagenPersonaje.src = "assets/pic/" + personajes[indice].foto;
	 imagenPersonaje.style.filter = "blur(20px)";

	 pregunta0.value = personajes[indice].preguntas[0];
	 pregunta0.disabled = true;

	 pregunta1.value = personajes[indice].preguntas[1];
	 pregunta1.disabled = true;

	 pregunta2.value = personajes[indice].preguntas[2];
	 pregunta2.disabled = true;

	 pregunta3.value = personajes[indice].preguntas[3];
	 pregunta3.disabled = true;

	 pregunta4.value = personajes[indice].preguntas[4];
	 pregunta4.disabled = true;

	btnJugar.disabled = true;  
});

const respuesta0 = document.getElementById("respuesta0");

respuesta0.addEventListener('change', () =>{

	if (respuesta0.value == personajes[indice].respuestas[0]) {

			opacidad = opacidad - 4;
			puntaje = puntaje + 1;

			imagenPersonaje.style.filter = "blur(" + opacidad + "px)";
			document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
			document.getElementById("icoRespuesta0").src = "assets/pic/si.png";

	} else {
		document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
		document.getElementById("icoRespuesta0").src = "assets/pic/no.png";
	}

	respuesta0.disabled = true;

});

const respuesta1 = document.getElementById("respuesta1");

respuesta1.addEventListener('change', () =>{

	if (respuesta1.value == personajes[indice].respuestas[1]) {

			opacidad = opacidad - 4;
			puntaje = puntaje + 1;

			imagenPersonaje.style.filter = "blur(" + opacidad + "px)";
			document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
			document.getElementById("icoRespuesta1").src = "assets/pic/si.png";

	} else {
		document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
		document.getElementById("icoRespuesta1").src = "assets/pic/no.png";
	}

	respuesta1.disabled = true;

});

const respuesta2 = document.getElementById("respuesta2");

respuesta2.addEventListener('change', () =>{

	if (respuesta2.value == personajes[indice].respuestas[2]) {

			opacidad = opacidad - 4;
			puntaje = puntaje + 1;

			imagenPersonaje.style.filter = "blur(" + opacidad + "px)";
			document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
			document.getElementById("icoRespuesta2").src = "assets/pic/si.png";

	} else {
		document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
		document.getElementById("icoRespuesta2").src = "assets/pic/no.png";
	}

	respuesta2.disabled = true;

});

const respuesta3 = document.getElementById("respuesta3");

respuesta3.addEventListener('change', () =>{

	if (respuesta3.value == personajes[indice].respuestas[3]) {

			opacidad = opacidad - 4;
			puntaje = puntaje + 1;

			imagenPersonaje.style.filter = "blur(" + opacidad + "px)";
			document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
			document.getElementById("icoRespuesta3").src = "assets/pic/si.png";

	} else {
		document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
		document.getElementById("icoRespuesta3").src = "assets/pic/no.png";
	}

	respuesta3.disabled = true;

});

const respuesta4 = document.getElementById("respuesta4");

respuesta4.addEventListener('change', () => {

	if (respuesta4.value == personajes[indice].respuestas[4]) {

			opacidad = opacidad - 4;
			puntaje = puntaje + 1;

			imagenPersonaje.style.filter = "blur(" + opacidad + "px)";
			document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
			document.getElementById("icoRespuesta4").src = "assets/pic/si.png";

	} else {
		document.getElementById("puntajee").innerHTML = "<center><h1>Puntaje:</H1><H2>"+ puntaje +"</H2></center>";
		document.getElementById("icoRespuesta4").src = "assets/pic/no.png";
	}

	respuesta4.disabled = true;

});

const btnRespuesta = document.getElementById("btnRespuesta");

btnRespuesta.addEventListener('click', () => {

	const RespuestaGeneral = document.getElementById("respuestaGeneral").value;

	console.log("respuesta General " + RespuestaGeneral);

	respuestaUsu = RespuestaGeneral.toLowerCase();

	validarRespuestas(respuestaUsu, indice);

	btnRespuesta.disabled = true;

});

//validaciones de respuesta

const validarRespuestas = (nombre, i) =>
{
    const imgResultado = document.getElementById("imgResultado");
    let bandera = false;
    personajes[i].name.forEach(nomPer =>
    {
        if (nombre == nomPer) 
        {
            bandera = true;
        }
    })
    if (bandera == true) 
    {
        imgResultado.src ="assets/pic/win.png";
        console.log("ganaste");
    }else{
        imgResultado.src ="assets/pic/gameOver.png";
        console.log("Perdiste");
    }
}





